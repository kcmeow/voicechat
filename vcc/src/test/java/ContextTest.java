import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kpfu.Main;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.stepan.contextlib.Context;
import ru.kpfu.stepan.contextlib.ContextFromReflection;

public class ContextTest {
    private Context context;

    @Before
    public void setContext(){
        context = new ContextFromReflection("ru.kpfu");
    }

    @Test
    public void contextShouldntBeEmpty() {
        context.getComponent(ClientMain.class, "client"); //Гарантированно существует!
    }

    @Test
    public void contextShouldReturnNullIfTheresNoObject() {
        Assert.assertNull(context.getComponent(Main.class, "noSuchObject"));
    }
}
