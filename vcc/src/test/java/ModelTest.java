import org.junit.Assert;
import org.junit.Test;
import ru.kpfu.dto.Message;
import ru.kpfu.model.Room;

public class ModelTest {

    @Test
    public void roomShouldNotThrowNPE() {
        Room room = new Room();
        Assert.assertNotNull(room.getClients());
    }

    @Test
    public void messageShouldNotThrowNPE() {
        Message m = new Message();
        Assert.assertNotNull(m.getPayload());
        Assert.assertNotNull(m.getHeader());
    }
}
