package ru.kpfu.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import ru.kpfu.App;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.logic.ControllersContainer;


import java.io.IOException;
import java.util.List;

import static ru.kpfu.App.CONTEXT;

public class RoomController {
    @FXML
    Label userCount;

    @FXML
    ListView users;

    @FXML
    TextArea connectionInfo;

    @FXML
    Label usernameLabel;

    private ObservableList<String> usersList = FXCollections.observableArrayList();

    public void initialize() {
        usernameLabel.textProperty().setValue(CONTEXT.getComponent(ClientMain.class, "client").username);
        users.setItems(usersList);
        CONTEXT.getComponent(ControllersContainer.class, "container").setRoomController(this);
    }

    public void setUsers(List<String> users, String message){
        Platform.runLater(() -> {
            connectionInfo.setText(connectionInfo.getText() + "\n" + message);
            usersList.setAll(users);
            userCount.textProperty().setValue(String.valueOf(users.size()));
        });
    }


    @FXML
    public void toRooms() throws IOException {
        CONTEXT.getComponent(ClientMain.class, "client").roomDisconect();
        App.setRoot("rooms");
    }
    @FXML
    public void logout() {
        CONTEXT.getComponent(ClientMain.class, "client").logout();
    }

    @FXML
    public void exit() throws Exception {
        logout();
        App.app.stop();
    }
}
