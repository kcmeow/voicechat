package ru.kpfu.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ru.kpfu.App;
import ru.kpfu.elements.HBoxCell;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.logic.ControllersContainer;
import ru.kpfu.model.Room;
import java.io.IOException;
import java.util.*;

import static ru.kpfu.App.CONTEXT;

public class RoomsController {

    @FXML
    Label usernameLabel;

    @FXML
    ListView<HBoxCell> rooms;

    @FXML
    ListView<String> users;

    @FXML
    Label userCount;



   private ObservableList<HBoxCell> list = FXCollections.observableArrayList();
   private ObservableList<String> usersList = FXCollections.observableArrayList();

    public void initialize() {
        usernameLabel.textProperty().setValue(CONTEXT.getComponent(ClientMain.class, "client").username);
        rooms.setItems(list);
        users.setItems(usersList);
        CONTEXT.getComponent(ControllersContainer.class, "container").setRoomsController(this);
        CONTEXT.getComponent(ClientMain.class, "client").getInfo();
    }

    @FXML
    public void logout() {
        CONTEXT.getComponent(ClientMain.class, "client").logout();
    }
    @FXML
    public void exit() throws Exception {
        logout();
        App.app.stop();
    }

    @FXML
    public void createRoom(){
       CONTEXT.getComponent(ClientMain.class, "client").createRoom();
    }

    public void setRooms(List<Room> rooms){
        Platform.runLater(() -> {
            list.clear();
            for (Room room : rooms) {
                list.add(new HBoxCell("Room " + room.getId(), "To the room", room.getId()));
            }
        });
    }
    public void setUsers(List<String> users){
        Platform.runLater(() -> {
            userCount.textProperty().setValue(String.valueOf(users.size()));
            usersList.clear();
            usersList.addAll(users);
        });
    }

}
