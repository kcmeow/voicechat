package ru.kpfu.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import ru.kpfu.App;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.logic.ControllersContainer;
import ru.kpfu.threads.ClientMessageHandler;
import ru.kpfu.threads.MicrophoneThread;

import java.io.IOException;
import java.util.List;

import static ru.kpfu.App.CONTEXT;

public class MainController {
    @FXML
    TextField username;
    @FXML
    TextField ip;
    @FXML
    TextField port;

    @FXML
    public void clickButton () {
        CONTEXT.getComponent(ClientMain.class, "client")
                .login(username.getText(), ip.getText(), Integer.parseInt(port.getText()));
    }

    public void initialize() {
        CONTEXT.getComponent(ControllersContainer.class, "container").setMainController(this);
        MicrophoneThread mt = CONTEXT.getComponent(ClientMain.class, "client").microphoneThread;
        if(mt != null) mt.stopped = true;
        ClientMessageHandler ct = CONTEXT.getComponent(ClientMain.class, "client").clientMessageHandler;
        if(ct != null) ct.stopped = true;
    }

    @FXML
    public void exit() throws Exception {
        App.app.stop();
    }

    public void allertLoginError() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Connection refused");
            alert.setContentText("This name is already used");

            alert.showAndWait();
        });
    }
}
