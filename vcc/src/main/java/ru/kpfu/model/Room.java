package ru.kpfu.model;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private int id;
    private List<String> clients;
    private long connections;

    public Room() {
        clients = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getClients() {
        return clients;
    }

    public void setClients(List<String> clients) {
        this.clients = clients;
    }

    public long getConnections() {
        return connections;
    }

    public void setConnections(long connections) {
        this.connections = connections;
    }
}
