package ru.kpfu;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.stepan.contextlib.Context;
import ru.kpfu.stepan.contextlib.ContextFromReflection;

import java.io.IOException;

public class App extends Application {

    private static double xOffset = 0;
    private static double yOffset = 0;
    private static Stage stage;

    private static Scene scene;
    public final static Context CONTEXT = new ContextFromReflection("ru.kpfu");;
    public static App app;

    @Override
    public void stop() throws Exception {
        CONTEXT.getComponent(ClientMain.class, "client").logout();
        Platform.exit();
    }

    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        app = this;
        stage.initStyle(StageStyle.UNDECORATED);
        scene = new Scene(loadFXML("main"));
        stage.setScene(scene);
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
                FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
                Parent p = fxmlLoader.load();
                p.setOnMousePressed(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        xOffset = event.getSceneX();
                        yOffset = event.getSceneY();
                    }
                });
                p.setOnMouseDragged(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        stage.setX(event.getScreenX() - xOffset);
                        stage.setY(event.getScreenY() - yOffset);
                    }
                });
                return p;
    }

    public static void main(String[] args) {
        launch();
    }

}