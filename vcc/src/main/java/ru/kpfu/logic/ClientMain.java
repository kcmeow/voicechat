package ru.kpfu.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.App;
import ru.kpfu.dto.Message;
import ru.kpfu.stepan.contextlib.Component;
import ru.kpfu.threads.ClientMessageHandler;
import ru.kpfu.threads.MicrophoneThread;

import java.io.*;
import java.net.Socket;
import java.util.Base64;
import java.util.HashMap;

@Component("client")
public class ClientMain {
    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;
    public String username;
    public boolean inRoom = false;
    public static boolean loggined = false;
    private ObjectMapper om = new ObjectMapper();

    public ClientMessageHandler clientMessageHandler;
    public MicrophoneThread microphoneThread;


    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            (clientMessageHandler = new ClientMessageHandler(reader, writer)).start();
            (microphoneThread = new MicrophoneThread()).start();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void login(String username, String ip, int port) {
        this.startConnection(ip, port);
        Message m = new Message();
        m.setHeader("login");
        m.getPayload().put("username", username);
        this.username = username;
        try {
            App.setRoot("rooms");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException();
        }
        loggined = true;
        getInfo();
    }

    public void getInfo() {
        try {
            writer.println(om.writeValueAsString(new Message("init_info", new HashMap<>())));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
    public void voiceMessage(byte[] targetData, int length) {
        Message m = new Message();
        m.setHeader("voice_message");
        m.getPayload().put("voice_message", (Base64.getEncoder().encodeToString(targetData)));
        m.getPayload().put("length", length);
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
    public void roomConnect(int id) {
        Message m = new Message();
        m.setHeader("connection");
        m.getPayload().put("room_id", id);
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
        inRoom = true;
    }
    public void roomDisconect() {
        Message m = new Message();
        m.setHeader("disconnection");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
        inRoom = false;
    }
    public void createRoom() {
        Message m = new Message();
        m.setHeader("create_room");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
    public void logout() {
        if (!loggined) return;
        if(inRoom) roomDisconect();
        Message m = new Message();
        m.setHeader("logout");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }

        //FULL LOGOUT
        username = null;
        writer.close();
        try {
            reader.close();
            clientSocket.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        clientMessageHandler.stopped = true;
        microphoneThread.stopped = true;
        try {
            App.setRoot("main");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        loggined = false;
    }
}
