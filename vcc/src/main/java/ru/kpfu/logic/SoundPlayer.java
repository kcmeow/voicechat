package ru.kpfu.logic;

import ru.kpfu.stepan.contextlib.Component;

import javax.sound.sampled.*;

@Component("soundPlayer")
public class SoundPlayer {

    private volatile SourceDataLine sourceLine;

    public SoundPlayer() {
        AudioFormat format = new AudioFormat(44100, 16, 1, true, false);
        DataLine.Info sourceInfo = new DataLine.Info(SourceDataLine.class, format);
        try {
            sourceLine = (SourceDataLine) AudioSystem.getLine(sourceInfo);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException(e);
        }
        try {
            sourceLine.open(format);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException(e);
        }
        sourceLine.start();
    }

    public void playSound(byte[] voice_message, int length) {
        try {
            sourceLine.write(voice_message, 0, length);
        }
        catch (Exception e) {
//            throw new IllegalStateException(e);
        }
    }
}
