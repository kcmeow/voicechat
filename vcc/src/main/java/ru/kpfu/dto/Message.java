package ru.kpfu.dto;

import java.util.HashMap;
import java.util.Map;

public class Message {
    private String header;
    private Map<String, Object> payload;

    public Message(){
        header = "";
        payload = new HashMap<>();
    }

    public Message(String header, Map<String, Object> payload) {
        this.header = header;
        this.payload = payload;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Map<String, Object> getPayload() {
        return payload;
    }

    public void setPayload(Map<String, Object> payload) {
        this.payload = payload;
    }
}
