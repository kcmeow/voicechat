package ru.kpfu.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import ru.kpfu.app.Main;
import ru.kpfu.logic.ControllersContainer;

import java.io.IOException;
import java.util.List;

import static ru.kpfu.app.Main.CONTEXT;

public class RoomController {
    @FXML
    Label userCount;

    @FXML
    ListView users;

    @FXML
    TextArea connectionInfo;
    private ObservableList<String> usersList = FXCollections.observableArrayList();

    public void initialize() {
        users.setItems(usersList);
        CONTEXT.getComponent(ControllersContainer.class, "container").setRoomController(this);
    }

    @FXML
    public void setUsers(List<String> users, String message){
        connectionInfo.setText(connectionInfo.getText() + "\n" + message);
        Platform.runLater(() -> {
            usersList.setAll(users);
            userCount.textProperty().setValue(String.valueOf(users.size()));
        });
    }


    @FXML
    public void toRooms() throws IOException {
        Main.setRoot("rooms");
    }
    @FXML
    public void exit() {

    }
}
