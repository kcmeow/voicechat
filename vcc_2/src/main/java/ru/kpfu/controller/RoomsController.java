package ru.kpfu.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import ru.kpfu.app.Main;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.logic.ControllersContainer;
import ru.kpfu.elements.HBoxCell;
import ru.kpfu.model.Room;

import java.io.IOException;
import java.util.List;

import static ru.kpfu.app.Main.CONTEXT;

public class RoomsController {

    @FXML
    Label usernameLabel;

    @FXML
    ListView<HBoxCell> rooms;

    @FXML
    ListView<String> users;

    @FXML
    Label userCount;



   private ObservableList<HBoxCell> list = FXCollections.observableArrayList();
   private ObservableList<String> usersList = FXCollections.observableArrayList();

    public void initialize() {
        usernameLabel.textProperty().setValue(CONTEXT.getComponent(ClientMain.class, "client").username);
        rooms.setItems(list);
        users.setItems(usersList);
        CONTEXT.getComponent(ControllersContainer.class, "container").setRoomsController(this);
    }

    @FXML
    public void logout() throws IOException {
        Main.setRoot("main");
    }
    @FXML
    public void exit() {
        System.out.println("exit");
    }

    @FXML
    public void createRoom(){
       CONTEXT.getComponent(ClientMain.class, "client").createRoom();
    }

    public void setRooms(List<Room> rooms){
        list.clear();
        for (Room room : rooms) {
            list.add(new HBoxCell("Room " + room.getId(), "To the room", room.getId()));
        }
    }
    public void setUsers(List<String> users){
        Platform.runLater(() -> {
            userCount.textProperty().setValue(String.valueOf(users.size()));
        });
        usersList.clear();
        usersList.addAll(users);
    }

}
