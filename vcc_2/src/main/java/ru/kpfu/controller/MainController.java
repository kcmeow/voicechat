package ru.kpfu.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import ru.kpfu.logic.ClientMain;
import ru.kpfu.threads.ClientMessageHandler;
import ru.kpfu.threads.MicrophoneThread;

import java.io.IOException;
import static ru.kpfu.app.Main.CONTEXT;

public class MainController {
    @FXML
    TextField username;
    @FXML
    TextField ip;
    @FXML
    TextField port;

    @FXML
    public void clickButton () throws IOException {
        CONTEXT.getComponent(ClientMain.class, "client")
                .login(username.getText(), ip.getText(), Integer.parseInt(port.getText()));
    }

    public void initialize() {
        MicrophoneThread mt = CONTEXT.getComponent(ClientMain.class, "client").microphoneThread;
        if(mt != null) mt.stopped = true;
        ClientMessageHandler ct = CONTEXT.getComponent(ClientMain.class, "client").clientMessageHandler;
        if(ct != null) ct.stopped = true;

    }
}
