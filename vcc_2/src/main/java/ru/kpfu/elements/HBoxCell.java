package ru.kpfu.elements;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import ru.kpfu.app.Main;
import ru.kpfu.logic.ClientMain;
import java.io.IOException;
import static ru.kpfu.app.Main.CONTEXT;

public class HBoxCell extends HBox {
    Label label = new Label();
    Button button = new Button();
    public HBoxCell(String labelText, String buttonText, int id) {
        super();
        label.setText(labelText);
        label.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(label, Priority.ALWAYS);
        button.setText(buttonText);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    Main.setRoot("room");
                    CONTEXT.getComponent(ClientMain.class, "client").roomConnect(id);
                } catch (IOException e) {
                }
            }
        });
        this.getChildren().addAll(label, button);
    }
}
