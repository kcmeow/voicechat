package ru.kpfu.threads;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.app.Main;
import ru.kpfu.dto.Message;
import ru.kpfu.logic.ControllersContainer;
import ru.kpfu.logic.SoundPlayer;
import ru.kpfu.model.Room;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static ru.kpfu.app.Main.CONTEXT;

public class ClientMessageHandler extends Thread{

    public boolean stopped;
    private BufferedReader reader;
    private PrintWriter writer;

    public ClientMessageHandler (BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    @Override
    public void run() {
        while (!stopped) {
            try {
                String message = reader.readLine();
                ObjectMapper om = new ObjectMapper();
                Message message1 = om.readValue(message, Message.class);
                //Можем получить только информацию о состоянии комнат, о состоянии пользователей, кусок звука
                if(message1.getHeader().equals("voice_message")) {
                    CONTEXT.getComponent(SoundPlayer.class, "soundPlayer")
                            .playSound(Base64.getDecoder().decode((String) message1.getPayload().get("voice_message")));
                }
                else if(message1.getHeader().equals("rooms")) {
                    System.out.println("rooms");
                    List<Room> rooms = om.convertValue(message1.getPayload().get("rooms")
                            , new TypeReference<List<Room>>(){});
                    //Listener
                    CONTEXT.getComponent(ControllersContainer.class, "container")
                            .getRoomsController().setRooms(rooms);
                }
                else if(message1.getHeader().equals("room_now")) {
                    System.out.println("room_now");
                    //Пользователи в той же комнате (это ~гарантировано)
                    List<String> users = (List<String>) message1.getPayload().get("users");
                    String connected = (String) message1.getPayload().get("connected");
                    String disconnected = (String) message1.getPayload().get("disconnected");
                    String conMessage = connected.equals("") ? disconnected + " отключился" : connected + " подключился";
                    //Listener
                    CONTEXT.getComponent(ControllersContainer.class, "container")
                            .getRoomController().setUsers(users, conMessage);
                }
                else if(message1.getHeader().equals("users_now")) {
                    System.out.println("users_now");
                    List<String> users = (List<String>) message1.getPayload().get("users");
                    users = users.stream()
                            .map(a -> (a.substring(0, a.lastIndexOf(':')) + " in room " + a.substring(a.lastIndexOf(':') + 1)))
                            .collect(Collectors.toList());
                    //Listener
                    CONTEXT.getComponent(ControllersContainer.class, "container")
                            .getRoomsController().setUsers(users);
                }
                else if(message1.getHeader().equals("response")) {
                    String messageString = (String) message1.getPayload().get("message");
                    //works!
                    if(messageString.equals("unavailable")) {
                        Main.setRoot("main");
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
