package ru.kpfu.threads;

import ru.kpfu.logic.ClientMain;

import javax.sound.sampled.*;

import static ru.kpfu.app.Main.CONTEXT;

public class MicrophoneThread extends Thread {

    public boolean stopped;
    private TargetDataLine targetLine;

    public MicrophoneThread() {
        AudioFormat format = new AudioFormat(44100, 16, 2, true, true);
        DataLine.Info targetInfo = new DataLine.Info(TargetDataLine.class, format);
        try {
            targetLine = (TargetDataLine) AudioSystem.getLine(targetInfo);
            targetLine.open(format);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException(e);
        }
        targetLine.start();
    }

    @Override
    public void run() {
        try {
            int numBytesRead;
            byte[] targetData = new byte[targetLine.getBufferSize() / 5];
            while (!stopped) {
                numBytesRead = targetLine.read(targetData, 0, targetData.length);
                System.out.println(numBytesRead);
                if(CONTEXT.getComponent(ClientMain.class, "client").inRoom) {
                    CONTEXT.getComponent(ClientMain.class, "client").voiceMessage(targetData);
                }
            }
            targetLine.close();
        }
        catch (Exception e) {
            System.err.println(e);
        }
    }
}
