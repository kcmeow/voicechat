package ru.kpfu.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.app.Main;
import ru.kpfu.threads.ClientMessageHandler;
import ru.kpfu.threads.MicrophoneThread;
import ru.kpfu.dto.Message;
import ru.kpfu.stepan.contextlib.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;
import java.util.HashMap;

@Component("client")
public class ClientMain {
    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;
    public String username;
    public boolean inRoom = false;
    public static boolean loggined = false;
    private ObjectMapper om = new ObjectMapper();

    public ClientMessageHandler clientMessageHandler;
    public MicrophoneThread microphoneThread;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            (clientMessageHandler = new ClientMessageHandler(reader, writer)).start();
            (microphoneThread = new MicrophoneThread()).start();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void login(String username, String ip, int port) {
        this.startConnection(ip, port);
        Message m = new Message();
        m.setHeader("login");
        m.getPayload().put("username", username);
        this.username = username;
        try {
            Main.setRoot("rooms");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        try {
            writer.println(om.writeValueAsString(m));
            writer.println(om.writeValueAsString(new Message("init_info", new HashMap<>())));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException();
        }
    }
    public void voiceMessage(byte[] targetData) {
        Message m = new Message();
        m.setHeader("voice_message");
        m.getPayload().put("voice_message", Base64.getEncoder().encode(targetData));
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
    public void roomConnect(int id) {
        Message m = new Message();
        m.setHeader("connection");
        m.getPayload().put("room_id", id);
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
        inRoom = true;
    }
    public void roomDisconect() {
        Message m = new Message();
        m.setHeader("disconnection");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
        inRoom = false;
    }
    public void createRoom() {
        Message m = new Message();
        m.setHeader("create_room");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
    public void logout() {
        Message m = new Message();
        m.setHeader("logout");
        try {
            writer.println(om.writeValueAsString(m));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }

        //FULL LOGOUT
        username = null;
        writer.close();
        try {
            reader.close();
            clientSocket.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        clientMessageHandler.stopped = true;
        microphoneThread.stopped = true;
        try {
            Main.setRoot("main");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
