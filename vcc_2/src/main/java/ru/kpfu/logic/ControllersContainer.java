package ru.kpfu.logic;

import ru.kpfu.controller.MainController;
import ru.kpfu.controller.RoomController;
import ru.kpfu.controller.RoomsController;
import ru.kpfu.stepan.contextlib.Component;

@Component("container")
public class ControllersContainer {
    MainController mainController;
    RoomsController roomsController;
    RoomController roomController;

    public ControllersContainer() {
        mainController = new MainController();
        roomsController = new RoomsController();
        roomController = new RoomController();
    }

    public RoomController getRoomController() {
        return roomController;
    }

    public void setRoomController(RoomController roomController) {
        this.roomController = roomController;
    }

    public MainController getMainController() {
        return mainController;
    }

    public RoomsController getRoomsController() {
        return roomsController;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setRoomsController(RoomsController roomsController) {
        this.roomsController = roomsController;
    }
}
