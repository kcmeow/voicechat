package ru.kpfu.logic;

import ru.kpfu.stepan.contextlib.Component;

import javax.sound.sampled.*;

@Component("soundPlayer")
public class SoundPlayer {

    private SourceDataLine sourceLine;

    public SoundPlayer() {
        AudioFormat format = new AudioFormat(44100, 16, 2, true, true);
        DataLine.Info sourceInfo = new DataLine.Info(SourceDataLine.class, format);
        try {
            sourceLine = (SourceDataLine) AudioSystem.getLine(sourceInfo);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException(e);
        }
        try {
            sourceLine.open(format);
        } catch (LineUnavailableException e) {
            throw new IllegalStateException(e);
        }
        sourceLine.start();
    }

    public void playSound(byte[] voice_message) {
        try {
            sourceLine.write(voice_message, 0, voice_message.length);
        }
        catch (Exception e) {
//            throw new IllegalStateException(e);
        }
    }
}
