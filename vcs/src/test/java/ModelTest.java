import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Test;
import ru.kpfu.dto.Message;
import ru.kpfu.dto.Room;
import ru.kpfu.dto.RoomSave;
import ru.kpfu.model.RoomsInfo;

public class ModelTest {
    @Test
    public void messageShouldNotThrowNPE() {
        Message m = new Message();
        Assert.assertNotNull(m.getPayload());
        Assert.assertNotNull(m.getHeader());
    }
    @Test
    public void dtoRoomShouldNotThrowNPE() {
        Room r = new Room();
        Assert.assertNotNull(r.getClients());
    }
    @Test
    public void roomSaveShouldNotThrowNPE() {
        RoomSave rs = new RoomSave();
        Assert.assertNotNull(rs.getId());
        Assert.assertNotNull(rs.getConnections());
    }
    @Test
    public void roomShouldNotThrowNPE() {
        ru.kpfu.model.Room r = new ru.kpfu.model.Room();
        Assert.assertNotNull(r.getClients());
    }
    @Test
    public void roomInfoShouldNotThrowNPE() {
        RoomsInfo ri = new RoomsInfo();
        Assert.assertNotNull(ri.getRooms());
    }
}
