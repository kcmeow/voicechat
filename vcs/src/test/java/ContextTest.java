import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kpfu.logic.MessageController;
import ru.kpfu.logic.ServerMain;
import ru.kpfu.stepan.contextlib.Context;
import ru.kpfu.stepan.contextlib.ContextFromReflection;

public class ContextTest {
    private Context context;

    @Before
    public void setContext(){
        context = new ContextFromReflection("ru.kpfu");
    }

    @Test
    public void contextShouldntBeEmpty() {
        context.getComponent(MessageController.class, "messageController"); //Гарантированно существует!
    }

    @Test
    public void contextShouldReturnNullIfTheresNoObject() {
        Assert.assertNull(context.getComponent(ServerMain.class, "noSuchObject"));
    }
}
