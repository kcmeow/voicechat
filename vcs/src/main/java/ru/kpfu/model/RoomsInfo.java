package ru.kpfu.model;

import ru.kpfu.dto.RoomSave;

import java.util.ArrayList;
import java.util.List;

public class RoomsInfo {
    int lastIndex;
    List<RoomSave> rooms;

    public RoomsInfo () {
        rooms = new ArrayList<>();
    }

    public int getLastIndex() {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    public List<RoomSave> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomSave> rooms) {
        this.rooms = rooms;
    }
}
