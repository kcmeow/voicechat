package ru.kpfu.model;

import ru.kpfu.threads.ClientHandler;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private List<ClientHandler> clients;
    private long connections;

    public Room () {
        clients = new ArrayList<>();
    }

    public Room(long connections) {
        this();
        this.connections = connections;
    }

    public List<ClientHandler> getClients() {
        return clients;
    }

    public void setClients(List<ClientHandler> clients) {
        this.clients = clients;
    }

    public long getConnections() {
        return connections;
    }

    public void setConnections(long connections) {
        this.connections = connections;
    }

    public void connect(ClientHandler client) {
        clients.add(client);
        connections++;
    }

    public void disconnect(ClientHandler client) {
        clients.remove(client);
    }
}
