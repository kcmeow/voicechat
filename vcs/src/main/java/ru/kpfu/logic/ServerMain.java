package ru.kpfu.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.dto.Message;
import ru.kpfu.dto.RoomSave;
import ru.kpfu.model.Room;
import ru.kpfu.model.RoomsInfo;
import ru.kpfu.stepan.contextlib.Context;
import ru.kpfu.stepan.contextlib.ContextFromReflection;
import ru.kpfu.threads.ClientHandler;
import ru.kpfu.threads.RoomsInfoAutosave;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ServerMain {
    private List<ClientHandler> clients;
    private Map<Integer, Room> rooms;
    public static final Context CONTEXT = new ContextFromReflection("ru.kpfu");
    public static int lastRoomIdex = 0;

    public ServerMain() {
        rooms = new ConcurrentHashMap<>();
        clients = new ArrayList<ClientHandler>();
        RoomsInfo roomsInfo;
        try {
            roomsInfo = (new ObjectMapper()).readValue(new File("rooms.json"), RoomsInfo.class);
            lastRoomIdex = roomsInfo.getLastIndex();
            for(RoomSave rs : roomsInfo.getRooms()) {
                rooms.put(rs.getId(), new Room(rs.getConnections()));
            }
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            System.out.println("Can't read rooms.json");
        }

        RoomsInfoAutosave roomsInfoAutosave = new RoomsInfoAutosave(rooms);
        roomsInfoAutosave.start();
    }

    public void start() {
        int port = -1;
        System.out.println("Введите порт сервера");
        Scanner sc = new Scanner(System.in);
        port = sc.nextInt();
            ServerSocket serverSocket;
            try {
                serverSocket = new ServerSocket(port);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            System.out.println("Server started");

            while (true) {
                ClientHandler handler = null;
                try {
                    handler = new ClientHandler(serverSocket.accept(), clients, rooms);
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                handler.start();
            }

        }
    }
