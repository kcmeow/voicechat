package ru.kpfu.logic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.dto.Message;
import ru.kpfu.stepan.contextlib.Component;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Component("messageController")
public class MessageController {

    public void sendMessage(PrintWriter pw, Message message) {
        try {
            pw.println((new ObjectMapper()).writeValueAsString(message));
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendResponse(PrintWriter pw, String errorMessage) {
        String header = "response";
        Map<String, Object> pl = new HashMap<>();
        pl.put("message", errorMessage);

        sendMessage(pw, new Message(header, pl));
    }
}
