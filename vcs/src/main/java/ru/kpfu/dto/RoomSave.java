package ru.kpfu.dto;

public class RoomSave {
    private int id;
    private long connections;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getConnections() {
        return connections;
    }

    public void setConnections(long connections) {
        this.connections = connections;
    }
}
