package ru.kpfu.threads;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.dto.Message;
import ru.kpfu.logic.MessageController;
import ru.kpfu.logic.ServerMain;
import ru.kpfu.model.Room;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.kpfu.logic.ServerMain.CONTEXT;

public class ClientHandler extends Thread {
    private Socket clientSocket;
    private String username = "";
    private ObjectMapper om;
    private int roomID = -1;

    private List<ClientHandler> clients;
    private Map<Integer, Room> rooms;


    public ClientHandler(Socket clientSocket, List<ClientHandler> clients, Map<Integer, Room> rooms) {
        om = new ObjectMapper();
        this.clientSocket = clientSocket;
        clients.add(this);
        this.clients = clients;
        this.rooms = rooms;
//        System.out.println("New client!");
    }


    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
            PrintWriter writer = new PrintWriter(
                    clientSocket.getOutputStream(), true);

            String line;

            while ((line = reader.readLine()) != null) {
                int id;
                Message message = om.readValue(line, Message.class);
                switch (message.getHeader()) {
                    case "login":
                        boolean error = false;
                        String name = (String) message.getPayload().get("username");
                        for (ClientHandler ch : clients) {
                            if(ch.equals(this)) continue;
                            else if(ch.username == null) {
                                clients.remove(ch);
                            }
                            if (ch.username.equals(name)) {
                                error = true;
                                CONTEXT.getComponent(MessageController.class, "messageController")
                                        .sendResponse(writer, "unavailable");
                                clientSocket.close();
                                clients.remove(this);
                                sendRoomsState();
                                sendUsersState();
                                break;
                            }
                        }
                        if (error) break;
                        username = name;
                        System.out.println("User " + name + " connected");
                        sendRoomsState();
                        sendUsersState();
                        break;
                    case "voice_message":
                        for (ClientHandler ch : rooms.get(roomID).getClients()) {
                            if (ch.equals(this)) continue;
                            PrintWriter pw = new PrintWriter(ch.clientSocket.getOutputStream(), true);
                            pw.println(line);
                        }
                        break;
                    //Подключение к комнате
                    case "connection":
                        id = (int) message.getPayload().get("room_id");
                        rooms.get(id).connect(this);
                        roomID = id;
                        sendRoomState(id, "connected", username);
                        sendRoomsState();
                        sendUsersState();
                        break;
                    //Отключение от комнаты
                    case "disconnection":
                        id = roomID;
                        if(id == -1) break;
                        rooms.get(id).disconnect(this);
                        roomID = -1;
                        sendRoomState(id, "disconnected", username);
                        sendRoomsState();
                        sendUsersState();
                        break;
                    //Создание комнаты
                    case "create_room":
                        id = ServerMain.lastRoomIdex++;
                        rooms.put(id, new Room());
                        sendRoomsState();
                        sendUsersState();
                        break;
                    //Отключение от сервера
                    case "logout":
                        if(roomID != -1) rooms.get(roomID).disconnect(this);
                        clientSocket.close();
                        clients.remove(this);
                        sendRoomsState();
                        sendUsersState();
                        break;
                    case "init_info":
                        sendRoomsState();
                        sendUsersState();
                        break;
                }
            }
        } catch (IOException e) {
            clients.remove(this);
            if (roomID != -1) {
                rooms.get(roomID).getClients().remove(this);
                sendRoomState(roomID, "disconnected", username);
            }
            System.out.println("User " + username + " disconnected");
        }
    }

    private void sendRoomState(int id, String label, String username) {
        Message message = new Message();
        message.setHeader("room_now");
        message.getPayload().put("users", rooms.get(id).getClients().stream().map(a -> a.username).collect(Collectors.toList()));
        message.getPayload().put(label, username);
        for (ClientHandler ch : rooms.get(id).getClients()) {
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(ch.clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            CONTEXT.getComponent(MessageController.class, "messageController")
                    .sendMessage(pw, message);
        }
    }

    private void sendUsersState() {
        Message message = new Message();
        message.setHeader("users_now");
        message.getPayload().put("users", clients.stream().map(a -> a.username + ":" + a.roomID).collect(Collectors.toList()));
        for (ClientHandler ch : clients) {
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(ch.clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            CONTEXT.getComponent(MessageController.class, "messageController")
                    .sendMessage(pw, message);
        }
    }
    private void sendRoomsStateToPW(PrintWriter writer) {
        Message message = new Message();
        message.setHeader("rooms");
        List<ru.kpfu.dto.Room> rooms1 = new ArrayList<>();
        for (Map.Entry<Integer, Room> room : rooms.entrySet()) {
            ru.kpfu.dto.Room r = new ru.kpfu.dto.Room();
            r.setClients(room.getValue().getClients().stream().map(a -> a.username).collect(Collectors.toList()));
            r.setId(room.getKey());
            r.setConnections(room.getValue().getConnections());

            rooms1.add(r);
        }
        message.getPayload().put("rooms", rooms1.stream().sorted((a, b) -> Math.toIntExact(b.getConnections() - a.getConnections())).collect(Collectors.toList()));
        CONTEXT.getComponent(MessageController.class, "messageController")
                .sendMessage(writer, message);
    }
    private void sendRoomsState() {
        for (ClientHandler ch : clients) {
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(ch.clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalStateException();
            }
            sendRoomsStateToPW(pw);
        }
    }
}
