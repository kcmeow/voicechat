package ru.kpfu.threads;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.kpfu.dto.RoomSave;
import ru.kpfu.logic.ServerMain;
import ru.kpfu.model.Room;
import ru.kpfu.model.RoomsInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Map;

public class RoomsInfoAutosave extends Thread {
    private Map<Integer, Room> rooms;

    public RoomsInfoAutosave(Map<Integer, Room> rooms) {
        this.rooms = rooms;
    }

    @Override
    public void run() {
        while (true) {
            RoomsInfo ri = new RoomsInfo();
            ri.setLastIndex(ServerMain.lastRoomIdex);

            for(Map.Entry<Integer, Room> roomEntry : rooms.entrySet()) {
                RoomSave rs = new RoomSave();
                rs.setId(roomEntry.getKey());
                rs.setConnections(roomEntry.getValue().getConnections());
                ri.getRooms().add(rs);
            }

            try {
                PrintWriter pw = new PrintWriter(new FileOutputStream(new File("rooms.json")));
                pw.println((new ObjectMapper()).writeValueAsString(ri));
                pw.flush();
                pw.close();
            } catch (JsonProcessingException | FileNotFoundException e) {
                throw new IllegalStateException(e);
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
