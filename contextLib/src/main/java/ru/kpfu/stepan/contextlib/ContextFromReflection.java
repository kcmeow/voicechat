package ru.kpfu.stepan.contextlib;

import org.reflections.Reflections;
import java.lang.reflect.Field;
import java.util.*;

public class ContextFromReflection  implements Context{
    private Map<String, Object> components;

    public ContextFromReflection(String prefix) {
        components = new HashMap<>();

        List<Class> compClasses = new ArrayList(new Reflections(prefix).getTypesAnnotatedWith(Component.class));
        for (Class comp : compClasses) {
            Object compon = null;
            try {
                compon = comp.getConstructor().newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }

            String name = compon.getClass().getAnnotation(Component.class).value();
            components.put(name, compon);
        }

        for (Map.Entry<String, Object> entry : components.entrySet()) {
            for (Field field : entry.getValue().getClass().getDeclaredFields()) {
                field.setAccessible(true);
                for (Map.Entry<String, Object> entry1 : components.entrySet()) {
                    if(field.getType().isAssignableFrom(entry1.getValue().getClass())) {
                        try {
                            field.set(entry.getValue(), entry1.getValue());
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                }
            }
        }
    }

    @Override
    public <T> T getComponent(Class<T> componentType, String name) {
        return (T) components.get(name);
    }
}
