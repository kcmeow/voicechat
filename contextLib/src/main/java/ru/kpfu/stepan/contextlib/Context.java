package ru.kpfu.stepan.contextlib;

public interface Context {
    <T> T getComponent (Class<T> classType, String name);
}
